	## Auto Ubuntu 18.05 AUTOINSTALL18.iso script ##
		### multibootusb installing multiple distros without user intervention
		### mkusb wipewholedevice
		### 
		### 
		### 
		### 
		### 
		### 
		### 
		### 
		### 
		### 
		### 
		### 
		### 


	# Created the /mnt/iso18 directory for editing.

sudo mkdir -p /mnt/iso18

	# Mounted Ubuntu /mnt/iso18 to copy the contents to the /mnt/iso18 directory and later edit the pertinent files for our auto install18

sudo mount -o loop ubuntu-18.04.2-desktop-amd64.iso /mnt/iso18

	# Created directory /opt/ubuntuiso18

sudo mkdir -p /opt/ubuntuiso18

	# Copied the ISO files to the /opt/ubuntuiso18 directory for editing.

sudo cp -rT /mnt/iso18 /opt/ubuntuiso18

	# Nano /opt/ubuntuiso18/isolinux/isolinux.cfg

sudo nano /opt/ubuntuiso18/isolinux/isolinux.cfg

	# Edited (ctrl+shift+V) the /opt/ubuntuiso18/isolinux/isolinux.cfg file and replaced everything inside with the following code ## (Removed extensions for 'kernel /casper/vmlinuz.efi' and 'initrd=/casper/initrd.lz'
		### Added this URL in this menu
		### https://gitlab.com/vcnlabs/open-source/ganeti/blob/master/preseed.cfg
		### url=https://gitlab.com/vcnlabs/open-source/ganeti/blob/master/preseed.cfg


default live-install
label live-install
  menu label ^Install Ubuntu
  kernel /casper/vmlinuz
  append  file=/cdrom/ks18.preseed auto=true priority=critical debian-installer/locale=en_US keyboard-configuration/layoutcode=us ubiquity/reboot=true languagechooser/language-name=English countrychooser/shortlist=US localechooser/supported-locales=en_US.UTF-8 boot=casper automatic-ubiquity initrd=/casper/initrd quiet splash noprompt noshell ---

	# Copied custom pressed file from the Desktop/opt/ubuntuiso18/ks.18.preseed to the root of the ubuntuiso18 inside the multi boot image.
		###----- Hashed out auto=install on the first line of the preseed file
		### ---- Using original preseed without modifications (23 July)
		### Fixed period in the new script from https://gitlab.com/vcnlabs/open-source/ganeti/blob/master/preseed.cfg (24 July)
		### Removed partitioning section from ks18.preseed (July 24) IT WORKED THIS WAY (1)
		### Added the partitioning section from the url https://gitlab.com/denny77/ganeti/raw/test_other_preseed/preseed.cfg (July 24)
		### Used the whole original https://gitlab.com/denny77/ganeti/raw/test_other_preseed/preseed.cfg (July 24)


sudo cp /home/volunteer/Desktop/ks18.preseed /opt/ubuntuiso18
sudo cp ks18.preseed /opt/ubuntuiso18
	# Created the new iso: -- auto install18.iso -- from the the /opt/ubuntuiso18/ directory.

sudo mkisofs -D -r -V ATTENDLESS_UBUNTU18 -cache-inodes -J -l -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -o /opt/autoinstall18.iso /opt/ubuntuiso18

	# Iso Hybrid command (to boot)
sudo isohybrid /opt/autoinstall18.iso
	
	# Single boot writing ISO image to target USB disk (will destroy data on USB disk):
		### Overriding the confirmation -y (July 24)

sudo multibootusb -c -r -i /opt/autoinstall18.iso -t -y /dev/sdb

	# multibootusb installing multiple distros without user intervention:
#sudo multibootusb -c -y -i /opt/autoinstall18.iso,/opt/autoinstall19.iso -t /dev/sdb1




	## CLEAN UP ##
	# Wipe the USB device (may take long time)
#sudo -H /usr/sbin/mkusb-11 wipe
	# ---- Wipe the first megabyte (MibiByte)
#sudo -H /usr/sbin/mkusb-11 wipe-whole-device

	# show only USB devices
#sudo -H /usr/sbin/mkusb-11 wipe-1

	# Removing the autoinstall18 and the ubuntuiso18 directories recursively
#sudo rm -rf /opt/autoinstall18.iso
#sudo rm -rf /opt/ubuntuiso18/ks18.preseed

#sudo rm -rf /opt/ubuntuiso18
#sudo rm -rf /mnt/iso18







	# Make USB (DONE)
	# https://help.ubuntu.com/community/mkusb

# sudo add-apt-repository universe  # only for standard Ubuntu
# sudo add-apt-repository ppa:mkusb/ppa  # and press Enter
# sudo apt-get update
# sudo apt-get install mkusb mkusb-nox usb-pack-efi
























